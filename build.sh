#!/bin/bash

# FF-Meta, a set of meta-packages used by FFOS to customize installs
# Copyright (C) 2020  Evan La Fontaine (FRC Team 4739, CTRL F5)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


. ./variables.conf
useradd builduser
userpasswd -d builduser

# insert string at the start of a the PKGBUILD followed by a new line
insert() {
    sed -i "1s@^@$1\n@" PKGBUILD
}

do_build() {
    # Generate addition package metadata
    insert "pkgname=ff-$name"
    insert "pkgver=$verMajor.$verMinor"
    insert "pkgrel=$verBuild"
    insert "arch=('x86_64')"
    insert "license=('GPL')"

    su -c "makepkg -g >> PKGBUILD" builduser
    su -c "PKGEXT=.pkg.tar.lz4 makepkg --noconfirm" builduser
}

find packages -type f -exec \
    sed -i "s@PRODUCTION_IP@$srv@g" {} +
find packages -type f -exec \
    sed -i "s@TESTING_IP@$srvTest@g" {} +

# make the packages in seperate folders to avoid collision
for package in packages/*
do
    name=$(basename -- "$package")

    if [[ -d $package ]]
    then
        cd packages
        chmod 777 $name
        cd $name

        if [ -f ./.install ]
        then
            insert "install=.install"
        fi
        do_build

        mv ./*.pkg.tar.lz4 ../../
        cd ..
    else
        mkdir $name
        chmod 777 $name
        mv $package $name/PKGBUILD
        cd $name

        do_build

        mv ./*.pkg.tar.lz4 ../
    fi
    cd ..
done